### Тестовое задание для Travelpayouts

## Install

```go get bitbucket.org/altmax/tpo-test```

## Build

```docker image build -t tpo-test:local .```

## Run 

```docker-compose up```

По умолчанию доступно по localhost(127.0.0.1) на порту 3333

```127.0.0.1:3333/proxy```

## Settings

В файле ```settings.json```

```
{
    "redis": { // адреса инстансов редиса
        "1": "redis:6379"
    },
    "http_port": "3333", // порт, который будет слушать http-сервер
    "cache_expiration": 3600, // длительно хранения кэша запросов, в секундах
    "link_to_classic_api": "https://places.aviasales.ru/v2/places.json", // ссылка на оригинальный сервис подсказок
    "timeout": 3 // максимальный таймаут ожидания ответа от оригинального сервиса подсказок, в секундах
}
```

## Benchmark

Использовать, например, Apache Benchmark:

```ab -n 10000 -c 10 127.0.0.1:3333/proxy?term=Москва&locale=en```