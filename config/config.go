package config

import (
	"encoding/json"
	"io"
)

// Config provides settings for server
type Config struct {
	Redis            map[string]string `json:"redis"` //multiple [name => address] for redis-ring
	HTTPPort         string            `json:"http_port"`
	CacheExpiration  int32             `json:"cache_expiration"` //in seconds
	LinkToClassicAPI string            `json:"link_to_classic_api"`
	Timeout          int32             `json:"timeout"` //max timeout for getting answer from classic api, in seconds
}

// ReadConfig parses json from Reader and returns Config
func ReadConfig(r io.Reader) (*Config, error) {
	c := &Config{}
	err := json.NewDecoder(r).Decode(c)
	if err != nil {
		return nil, err
	}
	return c, nil
}
