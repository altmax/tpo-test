package main

import (
	"flag"
	"log"
	"os"

	"bitbucket.org/altmax/tpo-test/config"
	"bitbucket.org/altmax/tpo-test/server"
	"github.com/go-redis/redis"
)

var (
	pathToConfig = flag.String("config", "config.json", "path/to/config.json")
)

func main() {
	f, err := os.Open(*pathToConfig)
	if err != nil {
		log.Fatalf("cannot open file with err: %v", err)
	}
	defer f.Close()

	conf, err := config.ReadConfig(f)
	if err != nil {
		log.Fatalf("cannot read config file with err: %v", err)
	}

	ring := redis.NewRing(&redis.RingOptions{
		Addrs: conf.Redis,
	})

	if err := ring.Ping().Err(); err != nil {
		log.Fatalf("redis not available with err: %v", err)
	}

	srv, err := server.New(conf, ring)
	if err != nil {
		log.Fatalf("cannot create new instance of server with err: %v", err)
	}

	if err := srv.Run(); err != nil {
		log.Fatalf("server run returned err: %v", err)
	}
}
