package server

// ClassicItem is a struct with useful info from classic api
type ClassicItem struct {
	Type        string `json:"type"`
	Code        string `json:"code"`
	CountryName string `json:"country_name,omitempty"`
	Name        string `json:"name"`
	CityName    string `json:"city_name,omitempty"`
}

// ClassicItems ...
type ClassicItems []ClassicItem

// NewItem is a struct for tp-widgets
type NewItem struct {
	Slug     string `json:"slug"`
	Subtitle string `json:"subtitle"`
	Title    string `json:"title"`
}

// NewItems ...
type NewItems []NewItem

// ToNewItems converts items
func (c ClassicItems) ToNewItems() NewItems {
	items := make([]NewItem, len(c))
	for i, v := range c {
		items[i] = v.toNewItem()
	}
	return items
}

func (c ClassicItem) toNewItem() NewItem {
	item := NewItem{
		Slug:  c.Code,
		Title: c.Name,
	}
	if c.Type == "city" {
		item.Subtitle = c.CountryName
	} else if c.Type == "airport" {
		item.Subtitle = c.CityName
	}
	return item
}
