package server

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	"bitbucket.org/altmax/tpo-test/config"
	"github.com/apsdehal/go-logger"
	"github.com/go-redis/cache"
	"github.com/go-redis/redis"
	"github.com/vmihailenco/msgpack"
)

// Server contains settings for http server
type Server struct {
	RedisRing        *redis.Ring
	HTTPPort         string
	CacheExpiration  time.Duration
	LinkToClassicAPI string
	Timeout          time.Duration
	l                *logger.Logger
}

// New creates new instance of server by received settings
func New(conf *config.Config, redisRing *redis.Ring) (*Server, error) {
	s := &Server{
		RedisRing:        redisRing,
		HTTPPort:         conf.HTTPPort,
		LinkToClassicAPI: conf.LinkToClassicAPI,
	}

	cacheExpiration := int32(3600)
	if conf.CacheExpiration > 0 {
		cacheExpiration = conf.CacheExpiration
	}
	s.CacheExpiration = time.Duration(cacheExpiration) * time.Second

	timeout := int32(3)
	if conf.Timeout > 0 {
		timeout = conf.Timeout
	}
	s.Timeout = time.Duration(timeout) * time.Second

	log, err := logger.New("test", 1, os.Stdout, logger.DebugLevel)
	if err != nil {
		return nil, err
	}
	s.l = log

	return s, nil
}

// Run starts http requests serving
func (s *Server) Run() error {
	codec := &cache.Codec{
		Redis: s.RedisRing,

		Marshal: func(v interface{}) ([]byte, error) {
			return msgpack.Marshal(v)
		},
		Unmarshal: func(b []byte, v interface{}) error {
			return msgpack.Unmarshal(b, v)
		},
	}

	http.HandleFunc("/proxy", s.handleWithRedis(codec))
	return http.ListenAndServe(":"+s.HTTPPort, nil)
}

func (s *Server) handleWithRedis(codec *cache.Codec) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		query := r.URL.Query().Encode()
		s.l.Infof("request query: %s", query)

		items := NewItems{}

		// if key not found make requetst to original service and cache response
		if err := codec.Get(query, &items); err != nil {
			s.l.Infof("key %s not found in redis", query)
			req, err := http.NewRequest(http.MethodGet, s.LinkToClassicAPI, nil)
			if err != nil {
				s.l.Errorf("new request answered with error: %v", err)
			}
			req.URL.RawQuery = query

			ctx, cancel := context.WithTimeout(context.Background(), s.Timeout)
			defer cancel()

			resp, err := http.DefaultClient.Do(req.WithContext(ctx))
			if err == nil {
				defer resp.Body.Close()

				func() {
					b, err := ioutil.ReadAll(resp.Body)
					if err != nil {
						s.l.Errorf("cannot readall from respnse body with err: %v", err)
						return
					}

					clcs := ClassicItems{}
					err = json.Unmarshal(b, &clcs)
					if err != nil {
						s.l.Errorf("cannot unmarshal response body with err: %v", err)
						return
					}

					items = clcs.ToNewItems()

					codec.Set(&cache.Item{
						Key:        query,
						Object:     items,
						Expiration: 1 * time.Second,
					})
					s.l.Infof("cache has been updated")
				}()
			} else {
				if ctx.Err() == context.DeadlineExceeded {
					s.l.Criticalf("classic api not answered in 3 seconds")
				} else {
					s.l.Errorf("get request returned error: %v", err)
				}
			}

		}

		b, err := json.Marshal(items)
		if err != nil {
			log.Fatal(err)
		}

		w.Write(b)
	}
}
