module bitbucket.org/altmax/tpo-test

require (
	github.com/apsdehal/go-logger v0.0.0-20180929234804-e1d9ff37593f
	github.com/go-redis/cache v6.3.5+incompatible
	github.com/go-redis/redis v6.15.2+incompatible
	github.com/vmihailenco/msgpack v4.0.4+incompatible
	google.golang.org/appengine v1.5.0 // indirect
)
