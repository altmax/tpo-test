FROM golang:1.12 as builder

WORKDIR /go/src/bitbucket.org/altmax/tpo-test
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build


FROM alpine:latest

RUN apk update && apk add --no-cache --virtual ca-certificates

WORKDIR /usr/share/zoneinfo
COPY --from=builder /usr/share/zoneinfo .

WORKDIR /root/
ENV TZ=UTC


COPY --from=builder /go/src/bitbucket.org/altmax/tpo-test/tpo-test .
COPY --from=builder /go/src/bitbucket.org/altmax/tpo-test/config.json .


ENTRYPOINT /root/tpo-test